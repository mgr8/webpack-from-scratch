require('./assets/stylesheets/styles.scss');


import React from 'react';
import ReactDOM from 'react-dom';

import app from './app/app.jsx'

ReactDOM.render(<app />, document.getElementById("root"));